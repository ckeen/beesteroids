;; Beesteroids - A small asteroids clone
;;
;; Copyright (c) 2012, Christian Kellermann
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:

;;     Redistributions of source code must retain the above copyright
;;     notice, this list of conditions and the following disclaimer.
;;
;;     Redistributions in binary form must reproduce the above
;;     copyright notice, this list of conditions and the following
;;     disclaimer in the documentation and/or other materials provided
;;     with the distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(use doodle srfi-1 matchable miscmacros sdl-mixer)

;; The basic thing has a coordinate, an facing angle, velocity and a bounding-radius which is used to check for collisions
;; The edges are  a list of coordinates representing the polygon to draw for our ship
;; The angle and heading values are stored in degrees
(define-record thing type pos velocity heading thrust bounding-radius edges color)

(define-record-printer (thing t out)
  (fprintf out "#,(~a ~a ~a ~a ~a ~a ~a ~a)"
           (thing-type t)
           (thing-pos t)
           (thing-velocity t)
           (thing-heading t)
           (thing-thrust t)
           (thing-bounding-radius t)
           (thing-edges t)
           (thing-color t)))

;; Our world holds information to enforce game rules.
;; Also bullets asteroids are lists holding these things, ship holds our ship
(define-record world level bullets-left lives score shield paused? bullets asteroids upgrades upgrade-timer ship)

;; Trigonometry helper functions

(define pi 3.14159265)

(define (deg->rad degs)
  (* degs (/ pi 180)))

(define (rad->deg rads)
  (* rads (/ 180 pi)))

(define (euclidian-distance p1 p2)
  (sqrt (+ (expt (- (first p1) (first p2)) 2)
           (expt (- (second p1) (second p2)) 2))))

;; Determines the coordinates of a point set forth from a starting point given an angle and distance
;; Used to find points for our asteroids
(define (radial-point-from p distance angle)
  (let ((x (first p))
        (y (second p)))
    (list (+ (* distance (sin (deg->rad angle))) x)
          (+ (* distance (cos (deg->rad angle))) y))))

;; Helper function to map our points to doodle's coordinates
(define (line pos1 pos2)
  (draw-line (car pos1) (cadr pos1) (car pos2) (cadr pos2)))

;; the size of our screen
(define *width* 800)
(define *height* 600)
;; The center of our screen
(define center `(,(/ *width* 2) ,(/ *height* 2)))

;; get us a random position on screen
(define (random-position)
  (list (random *width*)
        (random *height*)))

;; Let's have a brand new ship. These always start at the center of the screen
(define (make-ship #!key (angle (random 360)) heading  (pos center) (radius 30) velocity (speed 0) (thrust 0) (color '(0.8 0.8 0.8 1)))
  (let* ((h (or heading angle))
         (nose (radial-point-from pos radius h))
         (left (radial-point-from pos radius (- h 130)))
         (right (radial-point-from pos radius (+ h 130)))
         (tail  (radial-point-from pos (round (* radius 0.5)) (+ h 180)))
         (velocity (or velocity
                       (map - (radial-point-from pos speed angle) pos))))
    (make-thing 'ship pos velocity h thrust radius (list nose left tail right) color)))

;; Create a new bullet object, fire! uses this
(define (make-bullet pos angle speed)
  (let* ((nose (radial-point-from pos 10 angle))
         (left (radial-point-from pos 5 (- angle 210)))
         (tail (radial-point-from pos 8 (+ angle 180)))
         (right (radial-point-from pos 5 (+ angle 210)))
         (velocity (map - (radial-point-from pos speed angle) pos)))
    (make-thing 'bullet pos velocity 0 0 10 (list nose left tail right) '(1 1 1 1))))

;; Build some rocks
(define (make-asteroid pos radius n variation)
  (define (edges n)
    (let ((step (quotient 360 n)))
      (let loop ((n n)
                 (a 0)
                 (es '()))
        (if (zero? n)
            es
            (loop
             (sub1 n)
             (+ a step)
             (cons
              (radial-point-from pos (- radius (random variation)) a)
              es))))))
  (let* ((angle (random 360))
         (speed (max 0.1 (random 100)))
         (es (edges n))
         (velocity (map - (radial-point-from pos speed angle) pos)))
    (make-thing 'asteroid pos velocity 0 0 radius es '(0.3 0.3 0.3 1))))

(define (make-upgrade pos)
  (let ((es (list
             (radial-point-from pos 15 0)
             (radial-point-from pos 15 45)
             (radial-point-from pos 15 90)
             (radial-point-from pos 15 135)
             (radial-point-from pos 15 180)
             (radial-point-from pos 15 215)
             (radial-point-from pos 15 270)
             (radial-point-from pos 15 315))))
    (make-thing 'upgrade pos '(0 0) 0 0 10 es '(0.4 0 0 0.9))))

;; Move one thing around
(define (move-thing dt t)
  (let* ((pos (thing-pos t))
         (thrust  (* (/ dt 1000) (thing-thrust t)))
         (heading (thing-heading t))
         (velocity (thing-velocity t))
         (thrust-vec (map - (radial-point-from pos thrust heading) pos))
         (scaled-vel (map (cut * <> (/ dt 1000)) (map + velocity thrust-vec)))
         (new-vel  (map + velocity thrust-vec)))

    (thing-pos-set! t (map + pos scaled-vel))
    (thing-edges-set! t (map (lambda (e)
                               (map + e scaled-vel))
                             (thing-edges t)))
    (thing-velocity-set! t new-vel)))

;; Move objects around
(define (move-things time-delta . list-of-things)
  (for-each
   (lambda (things)
     (for-each
      (lambda (t)
        (move-thing time-delta t))
      things))
   list-of-things))

;; We rotate our ship by making a new one with the right heading
(define (rotate-ship ship delta-degrees)
  (let ((heading (thing-heading ship))
        (pos (thing-pos ship))
        (velocity (thing-velocity ship))
        (thrust (thing-thrust ship)))
    (make-ship heading: (modulo (+ heading delta-degrees) 360)
               pos: pos
               velocity: velocity
               thrust: thrust)))

;; This draws the polygon of a thing
(define (draw-thing thing)
  (filled-polygon (thing-edges thing) (thing-color thing) closed?: #f)
  (when *debugging*
        (let ((pos (thing-pos thing)))
          (circle (first pos) (second pos) (* 2 (thing-bounding-radius thing)) '(1 0 0 1))
          (line pos (map + (thing-velocity thing) pos)))))

;; Draw all things
(define (draw-things . list-of-things)
  (for-each (lambda (things)
              (for-each draw-thing things))
            list-of-things))

;; Put the world status on the screen
(define (draw-stats world dt)
  (font-color '(0 0.8 0 1))
  (font-size 20)
  (text 10 20
        (sprintf "Level: ~a "
                 (world-level world)))
  (when *paused*
    (text (car center) 20
          "- PAUSED -"
          align: #:center))

  (text (- *width* 10) 20
        (sprintf "~a upgrades ~a dt, ~a FPS" (world-upgrade-timer world) dt *fps*)
        align: #:right)
#;
  (unless (<= (world-shield world) 0)
          (text (- *width* 10) 20
                (sprintf "Shielded for ~a sec "
                         (truncate (world-shield world)))
                align: #:right))
  (text 10 (- *height* 10)
        (sprintf "Ships: ~a" (world-lives world)))
  (text (car center) (- *height* 10)
        "Left, Right, Down, [space] fires, [P]ause, [Q]uit"
        align: #:center)
  (text (- *width* 10) (- *height* 10)
        (sprintf "Points: ~a" (world-score world))
        align: #:right))


;; Check whether the object is outside the screen
(define (point-outside? pos)
  (let* ((x (truncate (first pos)))
         (y (truncate (second pos))))
    (or (< x 0)
        (>= x *width*)
        (< y 0)
        (>= y *height*))))

;; Remap points to the other edge of the screen
;;
;; Check which quadrant a point lies in
;;        |       |
;;    1   |   2   |  3
;;     ---+-------+---
;;        |       |
;;    4   |       |  5
;;        |       |
;;    ----+-------+---
;;        |       |
;;    6   |   7   |  8
;;
;; If the points lie in 3 quadrants, say 1, 2, 4 we have to use the
;; 6,3 axis to translate our object.
;;
;; If the points lie in quadrant 4, axis 2,7 is used
;; And if the points lie in quadrant 2, axis 4,5 is used
;;
;; Translations
;; |----------+---------------+---------------+-----------------+------------------|
;; | Quadrant | X translation | Y translation | condition for x | condition for y  |
;; |----------+---------------+---------------+-----------------+------------------|
;; |        1 | + *width*     | + *height*    | x < 0           | y < 0            |
;; |        2 | 0             | + *height*    | 0 < x < *width* | y < 0            |
;; |        3 | - *width*     | + *height*    | *width* < x     | y < 0            |
;; |        4 | + *width*     | 0             | x < 0           | 0 < y < *height* |
;; |        5 | - *width*     | 0             | *width* < x     | 0 < y < *height* |
;; |        6 | + *width*     | - *height*    | x < 0           | *height* < y     |
;; |        7 | 0             | - *heigth*    | 0 < x < *width* | *height* < y     |
;; |        8 | - *width*     | - *height*    | *width* < x     | *height* < y     |
;; |----------+---------------+---------------+-----------------+------------------|


(define (find-translation points)
  (let loop ((ps points)
             (tr '(0 0)))
    (if (null? ps) tr
        (let ((x (floor (caar ps)))
              (y (floor (cadar ps))))
          (loop (cdr ps)
                (list (if (<= x 0)
                          *width*
                          (if (< *width* x)
                              (- *width*)
                              (car tr)))
                      (if (<= y 0)
                          *height*
                          (if (< *height* y)
                              (- *height*)
                              (cadr tr)))))))))

(define (remap-all things)
  (for-each
   (lambda (t)
     (when (every point-outside? (thing-edges t))
       (let ((tr (find-translation (thing-edges t))))
         (thing-pos-set! t
                         (map + tr (thing-pos t)))
         (thing-edges-set! t
                           (map (lambda (p)
                                  (map + tr p))
                                (thing-edges t))))))
   things))


;; collision detection

;; To see whether a thing collides with another thing we measure the
;; distance between the positions and see whether that distance is bigger than both
;; bounding-radii
(define (collides? one another)
  (< (euclidian-distance (thing-pos one) (thing-pos another))
     (+ (thing-bounding-radius one)
        (thing-bounding-radius another))))

;; game logic

;; When the world allows it, create a new bullet and add it to the bullet list
(define (fire! world)
  (let* ((ship (world-ship world))
         (pos (thing-pos ship))
         (bullets-left (world-bullets-left world))
         (a (thing-heading ship))
         (r (thing-bounding-radius ship)))
    (when (< 0 bullets-left)
          (unless *muted* (play-sample *firing-sound*  duration: 1000))
          (world-bullets-left-set! world (sub1 bullets-left))
          (world-bullets-set! world
                              (cons (make-bullet (radial-point-from pos (+ r 10) a) a 200)
                                    (world-bullets world))))))

;; Start with a new world
(define (new-world #!key (level 1) (score 0) (lives 3) ship)
  (let ((as (list-tabulate
             level
             (lambda _ (make-asteroid (random-position) 50 15 10))))
        (s (or ship (make-ship))))
    (make-world level (+ (* 2 level) 5) lives score 20 #f '() as '() 0 s)))

;; what to do when we are hit
(define (ship-wrecked)
  (when (zero? (sub1 (world-lives *w*)))
    (set! *exit* #t))
  (set! *w*
        (new-world level: (world-level *w*) lives: (sub1 (world-lives *w*)) score: (world-score *w*))))

;; We hit something
(define (asteroid-destroyed a)
  (let* ((pos (thing-pos a))
         (r (thing-bounding-radius a))
         (a1 (list (+ (first pos) r)
                   (second pos)))
         (a2 (list (- (first pos) r)
                   (second pos)))
         (new-r (- r 10))
         (upgrade? (> (random 10) 7)))
    (unless *muted* (play-sample *explosion-sound* duration: 3000))
    (cond ((> new-r 20)
           (world-asteroids-set!
            *w*
            (cons (make-asteroid a1 new-r 20 10)
                  (cons (make-asteroid a1 new-r 20 10) (world-asteroids *w*)))))
          (upgrade? (world-upgrades-set!
                     *w*
                     (cons (make-upgrade pos) (world-upgrades *w*)))
                    (world-upgrade-timer-set! *w* 10))))
  (world-score-set! *w* (add1 (world-score *w*))))

;; Handle things that go off screen
(define (handle-outside-things world)

  (let ((as (world-asteroids world))
        (bullets (world-bullets world))
        (ship (world-ship world)))
    (remap-all as)
    (remap-all bullets)
    (remap-all (list ship))))

;; Handle collisions in our world
(define (check-collisions world)
  (let ((bullets (world-bullets world))
        (ship (world-ship world))
        (asteroids (world-asteroids world))
        (upgrades (world-upgrades world)))
    (cond ((find (cut collides? ship <>) asteroids) =>
           (lambda (a)
             (if (zero? (world-shield world))
                 (ship-wrecked)
                 (begin (world-asteroids-set! world (delete a asteroids))
                        (asteroid-destroyed a)))))
          ((find (cut collides? ship <>) upgrades) =>
           (lambda (u)
             (world-shield-set! world (+ 15 (world-shield world)))
             (world-upgrades-set! world (delete u upgrades))
             (when (null? (world-upgrades world))
                   (world-upgrade-timer-set! world 0)))))
    (for-each
     (lambda (b)
       (cond ((find (cut collides? b <>) asteroids) =>
              (lambda (a)
                (world-asteroids-set! world (delete a asteroids))
                (world-bullets-set! world (delete b bullets))
                (world-bullets-left-set! *w*
                                         (add1 (world-bullets-left *w*)))
                (asteroid-destroyed a)))))
     bullets)))

;; The actual doodle game loop starts here

;; First bring up a windown
(new-doodle width: *width* height: *height* background: "assets/space.png")

;; Our interaction state is just globally defined for now
(set! *exit* #f)
(set! *thrusting* #f)
(set! *rotating* #f)
(set! *debugging* #f)
(set! *paused* #f)
(set! *muted* #f)
(set! *start* (current-milliseconds))
(set! *time* (current-milliseconds))
(set! *frames* 0)
(set! *fps* "??")
(set! *thrust* 20)
(define *w*)

;; make us a global new world
(world-inits
 (lambda ()
   (open-audio)
   (set! *thrust-sound* (load-sample "assets/Rocket Thrusters-SoundBible.com-1432176431.wav"))
   (set! *firing-sound* (load-sample "assets/Bottle Rocket-SoundBible.com-332895117.wav"))
   (set! *explosion-sound* (load-sample "assets/Bomb_Exploding-Sound_Explorer-68256487.wav"))
   (set! *background-music* (load-music "assets/MyVeryOwnDeadShip.ogg"))
   (unless *muted*
     (play-music *background-music*))
   (music-finished (lambda () (play-music *background-music*)))
   (set! *w* (new-world level: 1))))

(world-changes
 (lambda (events dt quit)
   (inc! *frames*)
   (set! *time* (current-milliseconds))
   (set! *fps*
         (truncate (/ *frames* (/ (- (add1 *time*) *start*) 1000) )))
   (when (> (-  *time* *start*) 1000)
         (set! *frames* 0)
         (set! *start* *time*))
   (for-each
    (lambda (e)
      (match e
             (('key 'pressed #\q)
              (set! *exit* #t))
             (('key 'pressed #\d)
              (set! *debugging* #t))
             (('key 'released #\d)
              (set! *debugging* #f))
             (('key 'released #\p)
              (set! *paused* (not *paused*)))
             (('key 'pressed #\m)
              (set! *muted* (not *muted*))
              (if *muted*
                  (halt-music *background-music*)
                  (play-music *background-music*)))

             ;; Ship controls
             (('key 'pressed #\space)
              (unless *paused*
                (fire! *w*)))
             (('key 'pressed 'left)
              (set! *rotating* 5))
             (('key 'released 'left)
              (set! *rotating* #f))
             (('key 'pressed 'right)
              (set! *rotating* -5))
             (('key 'released 'right)
              (set! *rotating* #f))
             (('key 'pressed 'down)
              (set! *thrusting* #t))
             (('key 'released 'down)
              (set! *thrusting* #f))
             (else (void))))
    events)

   (clear-screen)

   (let* ((s (world-ship *w*))
          (pos (thing-pos s))
          (shield (world-shield *w*)))
     (cond (*thrusting*
            (thing-thrust-set! s (+ (thing-thrust s) (* *thrust* (/ dt 10))))
             (unless (and (not *muted*) (channel-playing? 1))
               (play-sample *thrust-sound* duration: 40 channel: 1)))
           (else
            (thing-thrust-set! s 0)))
     (when (> shield 0)
             (circle (first pos) (second pos) (* 2.5 (thing-bounding-radius s)) `(0 1 0 ,(* 0.1 shield)))
             (when (not *paused*)
                   (when (< 0 (world-upgrade-timer *w*))
                           (world-upgrade-timer-set!
                            *w*
                            (max 0 (- (world-upgrade-timer *w*) (/ dt 1000)))))
               (world-shield-set! *w* (max 0 (- shield (/ dt 1000)))))))

   (when (zero? (world-upgrade-timer *w*))
         (world-upgrades-set! *w* '()))

   (when (and (not *paused*) *rotating*)
     (world-ship-set!
      *w*
      (rotate-ship (world-ship *w*) (ceiling (* *rotating* (/ dt 10))))))

   (draw-things (world-asteroids *w*)
                (world-bullets *w*)
                (world-upgrades *w*)
                (list (world-ship *w*)))
   (draw-stats *w* dt)
   (show!)

   (unless *paused*
       (move-things dt
                    (world-asteroids *w*)
                    (world-bullets *w*)
                    (list (world-ship *w*)))

       (check-collisions *w*)
       (handle-outside-things *w*)

       (if (null? (world-asteroids *w*))
           (set! *w* (new-world level: (add1 (world-level *w*))
                                score: (world-score *w*)
                                lives: (world-lives *w*)
                                ship: (world-ship *w*)))))

   (when *exit* (quit #t))))

(world-ends
 (lambda ()
   (close-audio)
   (printf "You gained ~a points at level ~a~%" (world-score *w*) (world-level *w*))
   (print "Thanks for playing Beersteroids, see you soon!")))

(run-event-loop)
